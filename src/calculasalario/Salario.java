package calculasalario;

public class Salario {

    double horastrab;
    int numDepende;
    double salarioHor;

    private double salarioBruto;
    private double descINSS;
    private double descIR;
    private double descFGTS;
    private double salarioLiq;
    private double salarioFami;

    public Salario(double horastrab, int numDepende, double salarioHor) {
        this.horastrab = horastrab;
        this.numDepende = numDepende;
        this.salarioHor = salarioHor;
    }

    public void CalcularSalarios() {
        this.SalarioBruto(this.horastrab, this.salarioHor);
        this.descontoINSS();
        this.Descontoir();
        this.ValorFGTS(this.salarioBruto);
        this.calculoSalarioFamilia(this.numDepende, this.salarioBruto);
        this.salarioliquido();

    }

    //Salário Bruto
    public double SalarioBruto(double horasTrabalhada, double salarioPorHora) {
        this.horastrab = horasTrabalhada;
        this.salarioHor = salarioPorHora;

        this.salarioBruto = this.horastrab * this.salarioHor;

        return this.salarioBruto;

    }

    //Desconto INSS
    public void descontoINSS() {
        if (this.salarioBruto <= 1000) {
            this.descINSS = (this.salarioBruto * 0.085);
        } else if (this.salarioBruto > 1000) {
            this.descINSS = (this.salarioBruto * 0.09);
        }
    }

    //Desconto IR
    public void Descontoir() {

        if (this.salarioBruto > 500) {
            this.descIR = this.salarioBruto * 0.05;
            System.out.println("O desconto é" + this.descIR);
        } else {
            this.descIR = 0;
            System.out.println("Isento");
        }
    }

    //Desconto FGTS
    public double ValorFGTS(double salarioBruto) {
        double porcentagem = 0.08;
        double resposta = salarioBruto * porcentagem;
        this.descFGTS = resposta;
        return resposta;

    }

    //Salario Família
    public double calculoSalarioFamilia(int numeroDependentes, double salarioBruto) {
        this.salarioFami = 0;
        if (salarioBruto <= 877.67) {
            this.salarioFami = 45 * numeroDependentes;
        } else if (salarioBruto > 877.67 && salarioBruto <= 1319.18) {
            this.salarioFami = (double) (31.71 * numeroDependentes);
        } else if (salarioBruto > 1319.18) {
            this.salarioFami = 0;
        }
        return this.salarioFami;
    }

    //Salário Líquido
    public double salarioliquido() {

        double resultado = 0;

        resultado = this.salarioBruto - this.descINSS - this.descIR + this.salarioFami;
        this.salarioLiq = resultado;
        return resultado;

    }

    public double getHorastrab() {
        return horastrab;
    }

    public void setHorastrab(double horastrab) {
        this.horastrab = horastrab;
    }

    public int getNumDepende() {
        return numDepende;
    }

    public void setNumDepende(int numDepende) {
        this.numDepende = numDepende;
    }

    public double getSalarioHor() {
        return salarioHor;
    }

    public void setSalarioHor(double salarioHor) {
        this.salarioHor = salarioHor;
    }

    public double getSalarioBruto() {
        return salarioBruto;
    }

    public void setSalarioBruto(double salarioBruto) {
        this.salarioBruto = salarioBruto;
    }

    public double getDescINSS() {
        return descINSS;
    }

    public void setDescINSS(double descINSS) {
        this.descINSS = descINSS;
    }

    public double getDescIR() {
        return descIR;
    }

    public void setDescIR(double descIR) {
        this.descIR = descIR;
    }

    public double getDescFGTS() {
        return descFGTS;
    }

    public void setDescFGTS(double descFGTS) {
        this.descFGTS = descFGTS;
    }

    public double getSalarioLiq() {
        return salarioLiq;
    }

    public void setSalarioLiq(double salarioLiq) {
        this.salarioLiq = salarioLiq;
    }

    public double getSalarioFami() {
        return salarioFami;
    }

    public void setSalarioFami(double salarioFami) {
        this.salarioFami = salarioFami;
    }

}
